source "proxmox-iso" "packer_template" {
  
  proxmox_url = "${var.proxmox_url}"
  node = "${var.node}"
  username = "${var.token_id}"
  token = "${var.token_secret}"

  insecure_skip_tls_verify = "${var.insecure_skip_tls_verify}"

  vm_name = "${var.vm_name}"
  memory = "${var.memory}"
  cores = "${var.cores}"
  iso_file  = "${var.iso_file}"
  scsi_controller = "${var.scsi_controller}"

  disks {
    disk_size = "${var.disk_size}"
    storage_pool = "${var.storage_pool}"
    type = "${var.type}"
  }

  network_adapters {
    bridge = "${var.bridge}"
    model  = "${var.model}"
  }
  
  cloud_init = "${var.cloud_init}"
  cloud_init_storage_pool = "${var.cloud_init_storage_pool}"

  boot_command =  "${var.boot_command}"
  boot_wait = "${var.boot_wait}"

  unmount_iso = "${var.unmount_iso}"

  qemu_agent = "${var.qemu_agent}"

  ssh_username = "${var.ssh_username}"
  ssh_private_key_file = "${var.ssh_private_key}"
  ssh_timeout = "${var.ssh_timeout}"
}

build {
  sources = ["source.proxmox-iso.packer_template"]
}
