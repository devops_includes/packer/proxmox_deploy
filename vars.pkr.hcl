variable "proxmox_url" {
  type = string
}
variable "node" {
  type = string
}
variable "token_id" {
  type = string
}
variable "token_secret" {
  type = string
}

variable "vm_name" {
  type = string
  default = "Rocky-Template"
}

variable "iso_file" {
  type = string
  default = "iso-storage:iso/Rocky-8.8-x86_64-minimal.iso"
}

variable "insecure_skip_tls_verify" {
  type = bool
  default = true
}

variable "memory" {
  type = number
  default = 2048
}

variable "cores" {
  type = number
  default = 2
}

variable "scsi_controller" { 
  type = string
  default = "virtio-scsi-single"
}

variable "disk_size" {
  type = string
  default = "20G"
}

variable "storage_pool" {
  type = string
  default = "local-zfs"
}

variable "type" {
  type = string
  default = "scsi"
}

variable "bridge" {
  type = string
  default = "vmbr99"
}

variable "model" { 
  type = string
  default = "virtio"
}

variable "cloud_init" { 
  type = bool
  default = true
}

variable "cloud_init_storage_pool" {
  type = string
  default = "local-zfs"
} 

variable "boot_command" {
  type = list(string)
  default = ["<up><tab> ip=dhcp inst.cmdline inst.ks=https://gitlab.com/devops_includes/kickstarters/rocky_linux_8/-/raw/main/kickstarter.cfg<enter>"]
}

variable "boot_wait" { 
  type = string
  default= "5s"
}

variable "unmount_iso" {
  type = bool
  default = true
}

variable "qemu_agent" {
  type = bool
  default = true
}

variable "ssh_username" {
  type = string
  default = "root"
}
  
variable "ssh_timeout" {
  type = string
  default = "15m"
}

variable "ssh_private_key" {
  type = string
}
